import numpy as np
import classify
import os
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn import datasets
import sys

# classify.loo_crossvalidate()
# classify.scatter_plots()
#
#
# def get_rgb(file):
#     file = np.loadtxt(file, skiprows=1, usecols=(1,2,3))
#     mask = file.sum(axis=1)>0
#     r = file[:,0][mask].mean()
#     g = file[:,1][mask].mean()
#     b = file[:,2][mask].mean()
#     return np.array([r,g,b])
#
#
# def run(srcDir):
#     for directory in os.listdir(srcDir):
#         matrix = np.concatenate([get_rgb(dir + '\\' + file) for file in os.listdir(directory) if endswith(file, .txt)])
#         np.savetxt(dir+"rgb average", matrix, fmt="%.2f", newline='\n')
# run()
"""
join 3 training images
"""
def makelist_training(name_rgb, name_labels):
    name_rgb = os.path.join(sys.path[0], name_rgb)
    name_labels = os.path.join(sys.path[0], name_labels)
    x_tr__ = np.loadtxt(name_rgb)[1:, :]
    omega_tr_ = np.loadtxt(name_labels)
    return x_tr__, omega_tr_

def join_tr():
    x_tr_, omega_tr_ = makelist_training("averages training 1.txt", "labels_image training 1.txt")

    x_tr_new, omega_tr_new = makelist_training("averages training 2.txt", "labels_image training 2.txt")
    omega_tr_ = np.append(omega_tr_, omega_tr_new)
    x_tr_ = np.concatenate((x_tr_, x_tr_new))

    x_tr_new, omega_tr_new = makelist_training("averages training 3.txt", "labels_image training 3.txt")
    omega_tr_ = np.append(omega_tr_, omega_tr_new)
    x_tr_ = np.concatenate((x_tr_, x_tr_new))

    np.savetxt(os.path.join(sys.path[0],"rgb_training new.txt"), x_tr_, fmt="%u")
    np.savetxt(os.path.join(sys.path[0],"labels_training new.txt"), omega_tr_, fmt="%u")
    return print(x_tr_.shape, omega_tr_.shape)

# join_tr()

"""
run classifier on dataset 1
"""

# srcFile = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting'
srcFile = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI'
print(srcFile)
# dstFile = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code'
dstFile = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.4 Classification code'
print(dstFile)
ext = "average.txt" # Extension
# containString = "predicted_labels"
keepDirectories = True # Keep directory structure when saving

def run():
    srcDir=srcFile
    dstDir=dstFile
#     srcDir = srcFile.getAbsolutePath()
#     dstDir = dstFile.getAbsolutePath()
    for root, directories, filenames in os.walk(srcDir):
        absolute_predictions=np.zeros((1,3))
        absolute = []
        filenames.sort();
        if '.tifROI' in root:
            continue
        for filename in filenames:
            # Check for file extension
            if not filename.endswith(ext):
                continue
            if 'label' in filename:
                continue

            predicts = process(srcDir, dstDir, root, filename, keepDirectories)
            # absolute_predictions = np.append(absolute_predictions, np.asarray(predicts), axis=0)
            # absolute_predictions = np.concatenate((absolute_predictions, np.array(predicts)), axis=0)
            absolute.append(predicts)
            # absolute_predictions.append(predicts)
        # np.savetxt(root + 'label_predictions.txt', absolute_predictions, fmt="%u", newline='\n')
        # if not '.tifROI' in root:
        np.savetxt(root + 'label_predictions.txt', absolute, fmt="%u", newline='\n')
    return

def process(srcDir, dstDir, currentDir, fileName, keepDirectories):
    print("Processing: currentDir is ", currentDir)

    # Opening the image
    # print("Open image file", fileName, '\n hi')
    # print(os.path.join(currentDir, fileName))
    #   imp = IJ.openImage(os.path.join(currentDir, fileName))

    # Saving the image
    saveDir = currentDir.replace(srcDir, dstDir) if keepDirectories else dstDir
    if not os.path.exists(saveDir):
        os.makedirs(saveDir)

    # Put your processing commands here!
    absolute_predictions, omega_data_= classify.classify_dataset(os.path.join(currentDir, fileName))
    print(np.array(absolute_predictions).reshape((1,3)).shape)
    print("Saving to", saveDir)
    # os.rename(fileName,fileName.replace('.tifROIrgb average.txt',''))
    # fileName_old=os.path.join(currentDir,fileName)
    # os.rename(fileName_old, fileName_old.replace(ext_temp, 'average.txt'))
    np.savetxt(os.path.join(saveDir, fileName + 'predicted_labels.txt'), omega_data_, fmt="%u")
    return absolute_predictions

# ext_temp = '.txt'
# ext=ext_temp
srcFile = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI\OMF30 p11 overexpr. TWIST'
run()
