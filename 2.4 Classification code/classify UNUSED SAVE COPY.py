# Author: Romano van Genderen, 2020-02-09
# Any copyright is dedicated to the Public Domain.
# https://creativecommons.org/publicdomain/zero/1.0/

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn import datasets

"""
Loads pre-labeled training data
"""
def load_data(name_rgb='rgb_tr.txt', name_labels='labels_tr.txt'):
    x_tr__ = np.loadtxt(name_rgb)[1:, :]
    omega_tr_ = np.loadtxt(name_labels)
    if omega_tr_.shape[0] != x_tr__.shape[0]:
        print('Data incorrectly shaped for training, make sure you have as many points as labels')
    return x_tr__, omega_tr_

"""
Gets a (less is better) loss score for the labeled data set using leave-one-out crossvalidation
"""
def loo_crossvalidate():
    x_tr__, omega_tr_ = load_data()
    loss = 0
    for i in range(x_tr__.shape[0]):
        mask__ = np.ones_like(x_tr__)
        mask__[i, :] = 0
        mask__ = mask__.astype(bool)

        mask_om_ = np.ones_like(omega_tr_)
        mask_om_[i] = 0
        mask_om_ = mask_om_.astype(bool)

        logreg = LogisticRegression(C=1e5)
        x_tr_masked__ = x_tr__[mask__].reshape(-1, 3)
        omega_tr_masked_ = omega_tr_[mask_om_]
        logreg.fit(x_tr_masked__, omega_tr_masked_)

        x_pr_ = x_tr__[i, :]
        omega_pr = logreg.predict(x_pr_.reshape(1, -1))
        if omega_pr != omega_tr_[i]:
            loss += 1
    print(f'Loss score = {loss/x_tr__.shape[0]} (less is better)')
    return loss/x_tr__.shape[0]

"""
Plots RG, GB and BR scatter plots to study the color distribution across all cells
"""
def scatter_plots():
    x_tr__, omega_tr_ = load_data()
    fig, axes = plt.subplots(3, 1)
    for i, el in enumerate(['RG', 'GB', 'BR']):
        for j in range(3):
            axes[i].scatter(x_tr__[omega_tr_ == j, i%3], x_tr__[omega_tr_ == j, (i+1)%3], c=f'C{j}', label=f'Class {j}')
        axes[i].set_xlabel(el[0])
        axes[i].set_ylabel(el[1])
        axes[i].set_xlim([0, 255])
        axes[i].set_ylim([0, 255])
        axes[i].set_aspect('equal')
    plt.show()

"""
Gets file given by file_name and uses it, along with labeled data, to predict the labels for the unlabeled data in file_name
:param file_name: Data in .txt format containing RGB values
"""
def classify_dataset(file_name):
    x_tr__, omega_tr_ = load_data()
    logreg = LogisticRegression(C=1e5)
    logreg.fit(x_tr__, omega_tr_)
    x_data__ = np.loadtxt(file_name)[1:, :]
    omega_data_ = logreg.predict(x_data__)
    np.savetext('predicted_labels_' + file_name[4:], omega_data_)

if __name__ == '__main__':
    file_name = "*enter_file_name_here.txt*"
    classify_dataset(file_name)
