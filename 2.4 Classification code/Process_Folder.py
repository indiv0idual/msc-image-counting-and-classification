#@ File    (label = "Input directory", style = "directory") srcFile
#@ File    (label = "Output directory", style = "directory") dstFile
#@ String  (label = "File extension", value=".tif") ext
#@ String  (label = "File name contains", value = "") containString
#@ boolean (label = "Keep directory structure when saving", value = true) keepDirectories

# See also Process_Folder.ijm for a version of this code
# in the ImageJ 1.x macro language.


import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn import datasets
import os
import sys
import classify

def run():
  for root, directories, filenames in os.walk(srcDir):
    filenames.sort();
    for filename in filenames:
      # Check for file extension
      if not filename.endswith(ext):
        continue
      # Check for file name pattern
      if containString not in filename:
        continue
      process(srcDir, dstDir, root, filename, keepDirectories)

def process(srcDir, dstDir, currentDir, fileName, keepDirectories):
  # Put your processing commands here!
  label_numbers.append(classify.classify_dataset(os.path.join(srcDir,fileName)))
  # Saving the image
  saveDir = currentDir.replace(srcDir, dstDir) if keepDirectories else dstDir
  if not os.path.exists(saveDir):
      os.makedirs(saveDir)
  head,tail=os.path.split(srcDir)
  np.savetxt(os.path.join(saveDir,"label_numbers"+tail), label_numbers, fmt="%u")
  return

srcDir = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\counting I with ROI\OMF30 p11 overexpr. TWIST'
dstDir = r'C:\Users\048441\msc-image-counting-and-classification\2.4 Classification code\Dataset1.1'
keepDirectories=True
ext = ".txt"
containString="mean"
label_numbers = []
run()
print(label_numbers)
head,tail=os.path.split(srcDir)
np.savetxt(os.path.join(dstDir,"label_numbers"+tail), label_numbers, fmt="%u")
