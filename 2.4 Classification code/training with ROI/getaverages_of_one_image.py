import numpy as np
import sys, os
"""
"""

def get_RGB_raw(file):
    return np.loadtxt(file, skiprows=1, usecols=(1,2,3))
def remove_background_pixel_values(RGB_matrix):
    if (~RGB_matrix.any(axis=1)[0]):
        return np.delete(RGB_matrix, np.where(~RGB_matrix.any(axis=1))[0], axis=0)
    else:
        # print(RGB_matrix)
        return RGB_matrix
def avg_RGB(RGB_vector):
    return np.reshape(np.sum(RGB_vector, axis=0)/RGB_vector.shape[0], (1,3))
def avg_RGB_roi(filename):
    err = avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))
    return err


# Batch Processing an entire folder
def run(srcDir):
    matrix = np.zeros((1,3))
    for root, directories, filenames in os.walk(srcDir):
        for filename in sorted(filenames):
            if not filename.endswith(".txt"):
                continue
            matrix = np.concatenate((matrix, avg_RGB_roi(os.path.join(root,filename))))
    np.savetxt("RGB averaged values + {}.txt".format(filename), matrix, fmt="%.2f", newline='\n')
    return matrix

tr_nr=3

srcDir = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\training with ROI\quantification training 3.tifROI.zip'
print(os.path.join(sys.path[0], "fill cell {} with averaged color.ijm".format(tr_nr)))
# print(os.path.abspath(os.path.join(sys.path[0], "fill cell 1 with averaged color.ijm")))

run(srcDir)
