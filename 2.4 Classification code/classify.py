# Author: Romano van Genderen, 2020-02-09
# Any copyright is dedicated to the Public Domain.
# https://creativecommons.org/publicdomain/zero/1.0/

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn import datasets
import os
import sys

"""
Creates dictionary
"""
def empty_dict():
    image_ids='''1	1	1
1	1	2
1	1	3
1	1	4
1	1	5
2	1	1
2	1	2
2	1	3
2	1	4
2	1	5
1	2	1
1	2	2
1	2	3
1	2	4
1	2	5
2	2	1
2	2	2
2	2	3
2	2	4
2	2	5
1	3	1
1	3	2
1	3	3
1	3	4
1	3	5
2	3	1
2	3	2
2	3	3
2	3	4
2	3	5'''.replace('\t','-').split('\n')
    return dict.fromkeys(image_ids)

"""
If python not added to PATH, returns path of given file
"""
def get_path(file):
    return os.path.join(sys.path[0], file)

"""
Loads pre-labeled training data
"""
#def load_data(name_rgb='rgb_image p11 1-1-1.txt', name_labels='labels_image p11 1-1-1.txt'):
def load_data(name_rgb="rgb_training new.txt", name_labels="labels_training new.txt"):

    name_rgb = os.path.join(sys.path[0], name_rgb)
    name_labels = os.path.join(sys.path[0], name_labels)
    x_tr__ = np.loadtxt(name_rgb)
    omega_tr_ = np.loadtxt(name_labels)
    if omega_tr_.shape[0] != x_tr__.shape[0]:
        print('Data incorrectly shaped for training, make sure you have as many points as labels')
    return x_tr__, omega_tr_

"""
Loads a list of pre-labeled training data
"""
def load_data_list(name_rgb_list=['add', 'them', 'here', 'please', name_label_list=['add', 'them', 'here', 'please']):
    x_tr__ = np.concatenate(load_data(*i)[0] for i in zip(name_rgb_list, name_label_list))
    omega_tr = np.concatenate(load_data(*i)[1] for i in zip(name_rgb_list, name_label_list))
    return x_tr__, omega_tr_

"""
Gets a (less is better) loss score for the labeled data set using leave-one-out crossvalidation
"""
def loo_crossvalidate():
    x_tr__, omega_tr_ = load_data()
    loss = 0
    for i in range(x_tr__.shape[0]):
        mask__ = np.ones_like(x_tr__)
        mask__[i, :] = 0
        mask__ = mask__.astype(bool)

        mask_om_ = np.ones_like(omega_tr_)
        mask_om_[i] = 0
        mask_om_ = mask_om_.astype(bool)

        logreg = LogisticRegression(C=1e5)
        x_tr_masked__ = x_tr__[mask__].reshape(-1, 3)
        omega_tr_masked_ = omega_tr_[mask_om_]
        logreg.fit(x_tr_masked__, omega_tr_masked_)

        x_pr_ = x_tr__[i, :]
        omega_pr = logreg.predict(x_pr_.reshape(1, -1))
        if omega_pr != omega_tr_[i]:
            loss += 1
    print(f'Loss score = {loss/x_tr__.shape[0]} (less is better)')
    return loss/x_tr__.shape[0]

"""
Plots RG, GB and BR scatter plots to study the color distribution across all cells
"""
def scatter_plots():
    x_tr__, omega_tr_ = load_data()
    fig, axes = plt.subplots(3, 1)
    for i, el in enumerate(['RG', 'GB', 'BR']):
        for j in range(3):
            axes[i].scatter(x_tr__[omega_tr_ == j, i%3], x_tr__[omega_tr_ == j, (i+1)%3], c=f'C{j}', label=f'Class {j}')
        axes[i].set_xlabel(el[0])
        axes[i].set_ylabel(el[1])
        axes[i].set_xlim([0, 255])
        axes[i].set_ylim([0, 255])
        axes[i].set_aspect('equal')
    plt.show()

"""
Gets file given by file_name and uses it, along with labeled data, to predict the labels for the unlabeled data in file_name
:param file_name: Data in .txt format containing RGB values
"""
def classify_dataset(file_name):
    x_tr__, omega_tr_ = load_data()
    logreg = LogisticRegression(C=1e5)
    logreg.fit(x_tr__, omega_tr_)

    x_data__ = np.loadtxt(file_name)
    print(x_data__.shape)
    if (x_data__.ndim == 1):
        x_data__ = np.reshape(x_data__, (-1,3))
    # x_data__

    omega_data_ = logreg.predict(x_data__[1:,:]).astype(int)
    # print(omega_data_)

    purple = np.count_nonzero(omega_data_ == 1)
    blue = np.count_nonzero(omega_data_ == 2)
    pink = len(omega_data_) - purple - blue

    absolute_prediction = [pink, purple, blue]
    abs_prediction_dict = {'pink':pink, 'purple':purple, 'blue':blue}

    ''' last three numbers are the nr of labels in the image!'''
    omega_data_ = np.append(omega_data_, absolute_prediction)
    # print(omega_data_)
    head, file_name = os.path.split(file_name)
    # print(tail, " ", abs_prediction_dict)

    # make dict
    # OMF30P11_automatic[file_name[-9:].replace(".txt",'')] = absolute_prediction
    # print(OMF30P11_automatic)

    # np.savetxt(os.path.join(sys.path[0],('predicted_labels_' + fileName + '.txt')), omega_data_, fmt="%u")
    # np.savetxt(os.path.join(sys.path[0],('predicted_labels_' + fileName + '.txt')), omega_data_, fmt="%u")
    eventual = ['file_name ', absolute_prediction]
    return absolute_prediction, omega_data_
    # def main(input, output, file):
    #     return input, output, file_name

OMF30P11_automatic = empty_dict()
if __name__ == '__main__':
    # input = ""
    # output = ""

    file_name = "put something here.txt"
    # classify_dataset(file_name)
    loo_crossvalidate()
    scatter_plots()
