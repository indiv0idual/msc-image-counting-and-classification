%0 Journal Article
%A Caplan, Arnold I.

%T Mesenchymal stem cells
%D 1991
%J Journal of Orthopaedic Research
%P 641-650
%V 9
%N 5
%@ 0736-0266
%R 10.1002/jor.1100090504
%U https://onlinelibrary.wiley.com/doi/abs/10.1002/jor.1100090504
%X Abstract Bone and cartilage formation in the embryo and repair and turnover in the adult involve the progeny of a small number of cells called mesenchymal stem cells. These cells divide, and their progeny become committed to a specific and distinctive phenotypic pathway, a lineage with discrete steps and, finally, end-stage cells involved with fabrication of a unique tissue type, e.g., cartilage or bone. Local cuing (extrinsic factors) and the genomic potential (intrinsic factors) interact at each lineage step to control the rate and characteristic phenotype of the cells in the emerging tissue. The study of these mesenchymal stem cells, whether isolated from embryos or adults, provides the basis for the emergence of a new therapeutic technology of self-cell repair. The isolation, mitotic expansion, and site-directed delivery of autologous stem cells can govern the rapid and specific repair of skeletal tissues.




