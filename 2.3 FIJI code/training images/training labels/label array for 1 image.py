# labels image 1
import numpy as np

'''
training image 1
'''
label_vector = np.zeros((88,1), dtype=int) # most cells are pink anyways
#id_label0_pink

id_label1_green = [38, 39, 16, 65, 61, 49, 28, 34] # only a few
for id in id_label1_green:
    label_vector[id,0]=1

id_label2_blue = [10] # yeah it's empty
for id in id_label2_blue:
    label_vector[id,0]=2

label_file="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/labels_image training 1.txt"
np.savetxt(label_file, label_vector , newline='\n', fmt="%u")
##

'''
training image 2 id p11 1-1-2
'''
label_vector = np.zeros((93,1), dtype=int) # most cells are pink anyways
#id_label0_pink
index_label1_green = [69,73,66, 20, 22] # only a few
for index in index_label1_green:
    label_vector[index,0]=1

index_label2_blue = [0] # yeah it's empty
for index in index_label2_blue:
    label_vector[index,0]=2

label_file="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/labels_image training 2.txt"
np.savetxt(label_file, label_vector , newline='\n', fmt="%u")

'''
training image 4 id p11 1-3-4
'''
# label_vector = np.zeros((157,1), dtype=int) # most cells are pink anyways
# #id_label0_pink
#
# index_label1_green = [30,31,29,32,19,20,1,2,3,24] # only a few
# for index in index_label1_green:
#     label_vector[index,0]=1
#
# index_label2_blue = [2] # yeah it's empty
# for index in index_label2_blue:
#     label_vector[index,0]=2
#
# label_file="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/labels_image p11 1-1-2.txt"
# np.savetxt(label_file, label_vector , newline='\n')

'''
training image 3 id LAA OMF37 P9 24-05-19 siRNA well 2 photo 3
'''
label_vector = np.zeros((74,1), dtype=int) # most cells are pink anyways
#id_label0_pink

index_label1_green = [47,10,27, 34, 13, 12, 18, 21, 48, 46, 73, 56, 60]# only a few
for index in index_label1_green:
    label_vector[index,0]=1

index_label2_blue = [43, 44, 54, 45, 50, 16, 14, 65, 50]  # yeah it's empty
for index in index_label2_blue:
    label_vector[index,0]=2

label_file="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/labels_image training 3.txt"
np.savetxt(label_file, label_vector , newline='\n', fmt="%u")
