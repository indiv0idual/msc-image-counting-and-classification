
import numpy as np
import sys, os
"""
#@ File    (label = "Input directory", style = "directory") srcFile
#@ File    (label = "Output directory", style = "directory") dstFile
#@ String  (label = "File extension", value=".tif") ext
#@ String  (label = "File name contains", value = "") containString
#@ boolean (label = "Keep directory structure when saving", value = true)
"""
"""
Create a numpy vector with the RGB averages of one ROI
"""
def get_RGB_raw(file):
    return np.loadtxt(file, skiprows=1, usecols=(1,2,3))
def remove_background_pixel_values(RGB_matrix):
    if (~RGB_matrix.any(axis=1)[0]):
        return np.delete(RGB_matrix, np.where(~RGB_matrix.any(axis=1))[0], axis=0)
    else:
        # print(RGB_matrix)
        return RGB_matrix
def avg_RGB(RGB_vector):
    return np.reshape(np.sum(RGB_vector, axis=0)/RGB_vector.shape[0], (1,3))
def avg_RGB_roi(filename):
    err = avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))
    return err


def get_rgb(file):
    file = np.loadtxt(file, skiprows=1, usecols=(1,2,3))
    mask = file.sum(axis=1)>0
    r = file[:,0][mask].mean()
    g = file[:,1][mask].mean()
    b = file[:,2][mask].mean()
    return np.array([r,g,b])

def run0():
    matrix = np.zeros((1,3))
    for directory in os.listdir(srcDir):
        if directory.endswith(".zip"):
            print("hi", directory)
            os.rename(srcDir+'\\'+directory, srcDir+'\\'+directory.replace(".zip", ""))
            print(directory)
    for directory in os.listdir(srcDir):
        print(directory)
        directory = srcDir+'\\'+directory
        if directory + "rgb average.txt" in directory:
            continue
        if os.path.isfile(directory):
            continue
        if "quantification" in directory:
            matrix = np.concatenate([get_rgb(directory + '\\' + file) for file in os.listdir(directory) if file.endswith('.txt')])
        np.savetxt(directory+"rgb average.txt", matrix, fmt="%.2f", newline='\n')
    return matrix

def run():
    matrix = np.zeros((1,3))
    for directories in os.listdir(srcDir):
        directories = srcDir+'\\'+directories
        for directory in os.listdir(directories):
            if directory.endswith(".zip"):
                print("hi", directory)
                os.rename(directories+'\\'+directory, directories+'\\'+directory.replace(".zip", ""))
                print(directory)
        print("loop")
        for directory in os.listdir(directories):
            print(directory)
            directory = directories+'\\'+directory
            if directory + "rgb average.txt" in directory:
                continue
                # pass
            if os.path.isfile(directory):
                continue
            if "quantification" in directory:
                matrix = np.concatenate([get_rgb(directory + '\\' + file) for file in os.listdir(directory)])
            np.savetxt(directory+"rgb average.txt", matrix, fmt="%.2f", newline='\n')
    return

srcDir = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI'
#srcDir = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI\OMF37 p9 silencing TWIST'
dir= r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI\OMF30 p11 overexpr. TWIST'
srcDir = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI\OMF37 p5 silencing TWIST'
srcDir = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI\OMF37 p9 silencing TWIST'
srcDir = r'C:\Users\Isis Roling\Documents\Arielle\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting I with ROI\OMF39 p12 overexpr. TWIST'
run0()
