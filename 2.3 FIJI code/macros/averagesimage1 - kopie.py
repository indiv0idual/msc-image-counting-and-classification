#@ File    (label = "Input directory", style = "directory") srcFile

import numpy as np
import sys, os

#@ File    (label = "Input directory", style = "directory") srcFile
#@ File    (label = "Output directory", style = "directory") dstFile
#@ String  (label = "File extension", value=".tif") ext
#@ String  (label = "File name contains", value = "") containString
#@ boolean (label = "Keep directory structure when saving", value = true) keepDirectories

def get_RGB_raw(file):
    return np.loadtxt(file, skiprows=1, usecols=(1,2,3))
def remove_background_pixel_values(RGB_matrix):
    if (~RGB_matrix.any(axis=1)[0]):
        return np.delete(RGB_matrix, np.where(~RGB_matrix.any(axis=1))[0], axis=0)
    else:
        # print(RGB_matrix)
        return RGB_matrix
def avg_RGB(RGB_vector):
    return np.reshape(np.sum(RGB_vector, axis=0)/RGB_vector.shape[0], (1,3))
def avgadsfsfRGB(filename):
    #print(filename)
    err = avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))
    #print("err hi \n this is new threeliner average {} with dim {}".format(err, err.shape))
    return err
    #return avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))


# Batch Processing an entire folder
def run(srcDir):
    matrix = np.zeros((1,3))
    for root, directories, filenames in os.walk(srcDir):
        for filename in sorted(filenames):
            if not filename.endswith(".txt"):
                continue
            matrix = np.concatenate((matrix, avgadsfsfRGB(root+filename)))
    np.savetxt(srcDir+"RGB averaged values + {}.txt".format(filename), matrix, fmt="%.2f", newline='\n')
    return matrix


srcDir = r"C:\Users\048441\msc-image-counting-and-classification\2.3 FIJI code\RGB sets\\"
tr_nr = 2
print(os.path.join(sys.path[0], "fill cell {} with averaged color.ijm".format(tr_nr)))
# print(os.path.abspath(os.path.join(sys.path[0], "fill cell 1 with averaged color.ijm")))

run(srcDir, tr_nr)
