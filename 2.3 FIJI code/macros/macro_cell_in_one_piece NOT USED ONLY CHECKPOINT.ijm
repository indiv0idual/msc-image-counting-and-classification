// so here it goes

training_nr = "3"; //change this
image_training_name="LAA 100619 OMF30 p11 1-3-4"; //change this


image_training_segmented = "training "+training_nr;
image_training_nr="image "+training_nr;

inputfolderfx1 = "C:/Users/048441/msc-image-counting-and-classification/2.2 Watershed segmented images & code/";
//nameimagesegmented = "LAA 100619 OMF30 p11 1-1-1.out100.tif";
nameimagesegmented=image_training_segmented+".tif";
//outputfx1 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets "+image_training_nr;
outputfx1 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets training/";

ImageName=image_training_name;
//#@ File (label = "Input directory", style = "directory") input
//#@ File (label = "Output directory", style = "directory") output
//#@ String (label = "File suffix", value = ".tif") suffix


//open(inputfolderfx1+nameimagesegmented);
processfx1(inputfolderfx1,outputfx1,nameimagesegmented);


//#@ File (label = "Input directory", style = "directory") input
//#@ File (label = "Output directory", style = "directory") output
//#@ String (label = "File suffix", value = ".tif") suffix
print("hey test");
folder_clean= "C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/beta-gal counting/OMF30 p11 overexpr. TWIST/blinded/";
image_clean = image_training_name+".tif";
image_clean=folder_clean+image_clean;

inputFolderfx2= outputfx1+ImageName+"ROI";
print(inputFolderfx2);
outputFolderfx2 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/RGB sets/";
input2 = outputfx1+ImageName+"ROI.zip";
output2 = outputFolderfx2;
processfx2(input2, output2, image_clean);

processFolderfx2(inputFolderfx2,outputFolderfx2,image_clean);
print("hey test2");



//COUNT AND CREATE ROI SEGMENTS FOR 1 IMAGE
function processfx1(input, output, file) {
	// Do the processing here by adding your own code.
	//OPEN SEGMENTED IMAGE FROM PYTHON
	open(input+file);
	rename("segmented");
	//selectWindow("LAA 100619 OMF30 p11 1-1-1.out100.tif");
	run("Out [-]");
	run("Duplicate...", "title=binary copy 1");
	run("Out [-]");
	run("Make Binary");
	run("Analyze Particles...", "  show=Overlay exclude add");
	selectWindow("segmented");
	roiManager("Show None");
	roiManager("Show All");
	roiManager("Measure");
	//then in ROI manager select more>save>name of picture like below
	//roiManager("Save", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROIset_LAA 100619 OMF30 p11 1-1-1testcp[y.zip");
	//pathout="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets image1/";
	roiManager("deselect");
	input2=output+ImageName+"ROI.zip"
	roiManager("save", output + ImageName + "ROI.zip");
	//roiManager("save", output + ImageName);
	roiManager("Delete");

	print("Processing: " + input + File.separator + file);
	print("Saving to: " + output);
}

//MAKE RGB FILES FOR ALL ROI OF THE IMAGE

function processFolderfx2(input2, output2, image_clean) {
//function processFolderfx2(input2, output2, image_clean) {
	//output=outputFolderfx2;
	list = getFileList(input2);
	list = Array.sort(list);
	Array.print(list)
	suffix = ".roi" //this is kind of important though
	print("test processFolderfx2");
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input2 + File.separator + list[i]))
			processFolderfx2(input2 + File.separator + list[i], image_clean);
		if(endsWith(list[i], suffix))
			print("test 2 processFolderfx2");
			processfx2(input2, output2, list[i], i, image_clean);
			print("test 2 processFolderfx2 dit it do? processfxd");
	}

}

function processfx2(input2, output2, image_clean){

		open(image_clean);
		rename("original");
		run("Duplicate...", " ");
		rename("original copy");
		run("ROI Manager...");
		print("test enter processfx2");
	//1 CELL WITH AVERAGE COLOR
	roiManager("Open", input2);
	nROIs = roiManager("count");
	cell_index=0;
	for (i = 0; i <nROIs; i++) {
	roiManager("Select", i);
	//roiManager("Select", 0);

	run("Duplicate...", " ");
	rename("object "+i);
	run("Crop");
	selectWindow("object "+i);
	//run("In [+]");
	run("In [+]");
	//run("In [+]");
	run("Clear Outside"); //make outside black for pixel counting

		// NOW PLOT RGB VLAUES TO EXCEL
		//run("Average Color", "cielab");
		w = getWidth();
		h = getHeight();
		run("Clear Results");

		j = 0;
		for (x=0; x<w; x++) {
		 showProgress(x, w);
			for (y=0; y<h; y++) {
			v = getPixel(x, y);
			r = (v & 0xff0000)>>16;
			g = (v & 0x00ff00)>>8;
			b = (v & 0x0000ff);
			//NDVI = ((r-g)/(r+g));
			//SetResult("O", i, i);
			//setResult("Pr", i, x+1);
			//setResult("Pc", i, y+1);
			//setResult("NDVI", i, NDVI);
			setResult("R",j,r);
			setResult("G",j,g);
			setResult("B",j,b);
			j++;
			}
		}
		if (w==1 && h==1) {
			setResult("R",j,r);
			setResult("G",j,g);
			setResult("B",j,b);
		}
		updateResults();
		
		nameSubFile = "cell " + cell_index ;
		saveAs("Results", output2 + image_training_nr + nameSubFile + ".txt");
		//saveAs("Results", getDirectory("image")+"rgb__image1/cell1.txt");
		run("Clear Results");
		selectWindow("object "+i);
		close();
		cell_index+=1;

	// Leave the print statements until things work, then remove them.
	//print("Processing fx2: " + input2 + File.separator + file);
	print("Saving to: " + output2);
	}
}
roiManager("deselect")
roiManager("delete")
run("Clear Results")
run("Close All")
