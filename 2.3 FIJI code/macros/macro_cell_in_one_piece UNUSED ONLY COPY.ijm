setBatchMode(true);

//#@ File (label = "Input directory", style = "directory") input
//#@ File (label = "Output directory", style = "directory") output
//#@ String (label = "File suffix", value = ".tif") suffix


suffix=".tif";
//input = "C:/Users/048441/msc-image-counting-and-classification/2.2 Watershed segmented images & code/";
//output=input;
//processFolder(input);



#@ File (label = "Input directory ROI", style = "directory") iinput1
#@ File (label = "Input directory clean image", style = "directory") iinput_clean
suffix = "ROI.zip";
print("iinput is "+iinput1);
//iinput;
print("iinput is "+iinput1);
input1 = replace(iinput1, "\\", "/") + "/";
input_clean = replace(iinput_clean, "\\", "/") + "/";
print("input roi after replacement is " +input1);
print("input clean after replacement is " +input_clean);
hi=0;                                                        
containString="Counter";
//input1=input+File.separator+counting I with ROI+OMF30 p11 overexpr. TWIST+blinded
//input_clean=C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting\OMF30 p11 overexpr. TWIST\blinded
processFolderfx2(input1, input_clean);
//processfx2(input1, input_clean, image_clean);
input1 = File.getParent(input1) + "/OMF30 p11 overexpr. TWIST";
input_clean = File.getParent(File.getParent(input_clean)) + "/blinded/OMF30 p11 overexpr. TWIST";
processFolderfx2(input1, input_clean);

//MAKE RGB FILES FOR ALL ROI OF THE IMAGE
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list);

	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input + File.separator + list[i])){
			if(indexOf(list[i], "ROI") >= 0){
				print("hello");
				continue;
			}
			print("something is happening 1");			
			processFolder(input + File.separator + list[i]);
		}
		if(endsWith(list[i], suffix))
			processfx1(input, input, list[i]);
	}

}



//COUNT AND CREATE ROI SEGMENTS FOR 1 IMAGE
function processfx1(input, output, file) {
	// Do the processing here by adding your own code.
	//OPEN SEGMENTED IMAGE FROM PYTHON
	open(input+file);
	rename("segmented");
	run("Canvas Size...", "width=2046 height=1530 position=Center");
	//selectWindow("LAA 100619 OMF30 p11 1-1-1.out100.tif");
	run("Out [-]");
	run("Duplicate...", "title=binary copy 1");
	run("Out [-]");
	run("Make Binary");
	run("Analyze Particles...", "size=50-Infinity  show=Overlay include add");
	selectWindow("segmented");
	roiManager("Show None");
	roiManager("Show All");
	roiManager("Measure");
	roiManager("deselect");
//	input2=output+ImageName+"ROI.zip"
	roiManager("save", output + file + "ROI.zip");
	//roiManager("save", output + ImageName);
	roiManager("Delete");

	//print("Processing: " + input + File.separator + file);
	//print("Saving to: " + output);
}

//MAKE RGB FILES FOR ALL ROI OF THE IMAGE
function processFolderfx2(input1, input_clean) {
	list = getFileList(input1);
	list = Array.sort(list);
//	Array.print(list)
//	suffix = ".roi" //this is kind of important though
	print("test processFolderfx2");
	for (i = 0; i < list.length; i++) {
		if(indexOf(list[i], containString) >= 0){
				print("hello");
				continue;
			}
		if (endsWith(list[i], ".tif"))
			continue;
		if(endsWith(list[i], "ROI.zip")){
			print("test 2 processFolderfx2");
			processfx2(input1, input_clean, list[i]);
			print("test 2 processFolderfx2 dit it do? processfxd");
		}
	}

}

function processfx2(input_ROI, input_clean, file){
	roiManager("Open", input_ROI+file);

	file_name = replace(file, suffix, "");
	print(input_clean+file_name);
	open(input_clean+file_name);
	rename("original");
	run("Duplicate...", " ");
	rename("original copy");
	run("ROI Manager...");
	print("test enter processfx2");
		
	//1 CELL WITH AVERAGE COLOR
//	roiManager("Open", input_ROI+file);
//	selectWindow("ROImanager");
	nROIs = roiManager("count");
	cell_index=0;
	output = input_ROI + "quantification " + list[i] ;
	File.makeDirectory(output);
	for (i = 0; i <nROIs; i++) {
	roiManager("Select", i);
	//roiManager("Select", 0);
	run("Duplicate...", " ");
	rename("object "+i);
	run("Crop");
	selectWindow("object "+i);
	//run("In [+]");
	run("In [+]");
	//run("In [+]");
	run("Clear Outside"); //make outside black for pixel counting

		// NOW PLOT RGB VLAUES TO EXCEL
		//run("Average Color", "cielab");
		w = getWidth();
		h = getHeight();
		run("Clear Results");

		j = 0;
		for (x=0; x<w; x++) {
		 showProgress(x, w);
			for (y=0; y<h; y++) {
			v = getPixel(x, y);
			r = (v & 0xff0000)>>16;
			g = (v & 0x00ff00)>>8;
			b = (v & 0x0000ff);
			//NDVI = ((r-g)/(r+g));
			//SetResult("O", i, i);
			//setResult("Pr", i, x+1);
			//setResult("Pc", i, y+1);
			//setResult("NDVI", i, NDVI);
			setResult("R",j,r);
			setResult("G",j,g);
			setResult("B",j,b);
			j++;
			}
		}
		if (w==1 && h==1) {
			setResult("R",j,r);
			setResult("G",j,g);
			setResult("B",j,b);
		}
		updateResults();
		
		nameSubFile = "/cell " + cell_index ;
		saveAs("Results", output + nameSubFile + ".txt");
		run("Clear Results");
		selectWindow("object "+i);
		close();
		cell_index+=1;

	// Leave the print statements until things work, then remove them.
	//print("Processing fx2: " + input2 + File.separator + file);
//	print("Saving to: " + output2);
	}
		roiManager("deselect");
		roiManager("delete");
}
//roiManager("deselect")
//roiManager("delete")
//run("Clear Results")
//run("Close All")