import numpy as np
import os

def get_RGB_raw(file):
    return np.loadtxt(file, skiprows=1, usecols=(1,2,3))
def remove_background_pixel_values(RGB_matrix):
    if (~RGB_matrix.any(axis=1)[0]):
        return np.delete(RGB_matrix, np.where(~RGB_matrix.any(axis=1))[0], axis=0)
    else:
        print(RGB_matrix)
        return RGB_matrix
def avg_RGB(RGB_vector):
    return np.reshape(np.sum(RGB_vector, axis=0)/RGB_vector.shape[0], (1,3))
def avgadsfsfRGB(filename):
    print(filename)
    err = avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))
    print("err hi \n this is new threeliner average {} with dim {}".format(err, err.shape))
    return err
    #return avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))


def run(srcDir):
    matrix = np.zeros((1,3))
    print(matrix)
    for root, directories, filenames in os.walk(srcDir):
        #filenames = filter(None, filenames)
        #filenames = [x for x in filenames if x]
        #print(filenames, "hi")
        for filename in filenames:
            if not filename.endswith(".txt"):
                continue
            if "cell" not in filename:
                continue
            print("test is it concatenating? ")
            matrix = np.concatenate((matrix, avgadsfsfRGB(root+filename)))
            print("shape of matrix updated is {} ".format(matrix.shape))
    return matrix

srcDir = r"C:\Users\048441\msc-image-counting-and-classification\2.3 FIJI code\rgb sets image1\\"
run(srcDir)
