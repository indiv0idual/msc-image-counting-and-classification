selectWindow("LAA 100619 OMF30 p11 1-1-1.tif");
run("Out [-]");
run("Duplicate...", "title=[binary mask ]");
run("Out [-]");
run("Gaussian Blur...", "sigma=4");
run("8-bit");
run("Threshold...");
setAutoThreshold("Otsu");
//run("Duplicate...", " ");
//run("Out [-]");
run("Convert to Mask");
run("Duplicate...", "title=[distance map]");
run("Out [-]");
run("Distance Map");
run("Find Maxima...", "prominence=10 strict output=[Single Points]");
run("Out [-]");
run("Out [-]");
run("Find Maxima...", "prominence=10 strict output=Count");

function reconstruct(){
	run("Options...", "iterations=1 count=1 black do=Nothing");
	oldmean=0;
	getStatistics(area,mean);
	while(mean != oldmean){
		oldmean = mean;		
		run("Dilate");
		imageCalculation("AND", "Boundary", "Objects");
		getStatistics(area, mean);
}
}

selectWindow("LAA 100619 OMF30 p11 1-1-1.tif");
run("Duplicate...", " ");
run("Out [-]");
run("Gaussian Blur...", "sigma=4");
run("8-bit");
run("Duplicate...", " ");
run("Gray Morphology", "radius=3 type=square show operator=dilate");
imageCalculator("Subtract create", "LAA 100619 OMF30 p11 1-1-3.tif","LAA 100619 OMF30 p11 1-1-2.tif");

