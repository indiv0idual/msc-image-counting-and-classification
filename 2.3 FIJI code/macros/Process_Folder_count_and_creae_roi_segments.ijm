/*
 * Macro template to process multiple images in a folder
 */

///////inserted
//inputfolder = "C:/Users/048441/msc-image-counting-and-classification/2.2 Watershed code/";
//nameimage = "LAA 100619 OMF30 p11 1-1-1.out100.tif";
//outputfolder = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets image1";
/////////////////////


#@ File (label = "Input directory", style = "directory") input
#@ File (label = "Output directory", style = "directory") output
#@ String (label = "File suffix", value = ".tif") suffix


// See also Process_Folder.py for a version of this code
// in the Python scripting language.

processFolder(input);

// function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input + File.separator + list[i]))
			processFolder(input + File.separator + list[i]);
		if(endsWith(list[i], suffix))
			processFile(input, output, list[i]);
	}
}

function processFile(input, output, file) {
	// Do the processing here by adding your own code.
	//open clean original image from dataset folder

open(input+file);
selectWindow("LAA 100619 OMF30 p11 1-1-1.out100.tif");
run("Out [-]");
run("Duplicate...", "title=binary copy 1");
run("Out [-]");
run("Make Binary");
run("Analyze Particles...", "  show=Overlay exclude add");
selectWindow("LAA 100619 OMF30 p11 1-1-1.out100.tif");
roiManager("Show None");
roiManager("Show All");
roiManager("Measure");
//then in ROI manager select more>save>name of picture like below
//roiManager("Save", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROIset_LAA 100619 OMF30 p11 1-1-1testcp[y.zip");
pathout="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets image1/";
roiManager("deselect");
roiManager("save", pathout+"testing.zip");

//run("script:C:\Users\048441\msc-image-counting-and-classification\2.3 FIJI code\count segmented cells.ijm.ijm.ijm");
//Table.applyMacro("Sin=sin(row*0.1); Cos=cos(row*0.1); Sqr=Sin*Sin+Cos*Cos; ");
//roiManager("Select", 0);
//run("Image to Results"); what the heck does that number mean?
// need to detect RGB pixel values of segmented cells


	
	// Leave the print statements until things work, then remove them.
	print("Processing: " + input + File.separator + file);
	print("Saving to: " + output);
}
