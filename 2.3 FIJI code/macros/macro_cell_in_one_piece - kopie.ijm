// so here it goes

training_nr = "1";
image_training_name="LAA 100619 OMF30 p11 1-1-1";


image_training_segmented = "training "+training_nr;
image_training_nr="image "+training_nr;

inputfolderfx1 = "C:/Users/048441/msc-image-counting-and-classification/2.2 Watershed code/";
//nameimagesegmented = "LAA 100619 OMF30 p11 1-1-1.out100.tif";
nameimagesegmented=image_training_segmented+".tif";
//outputfx1 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets "+image_training_nr;
outputfx1 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets training/";

ImageName=image_training_name;
//#@ File (label = "Input directory", style = "directory") input
//#@ File (label = "Output directory", style = "directory") output
//#@ String (label = "File suffix", value = ".tif") suffix

//inputfolderfx2 = "C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/beta-gal counting/OMF30 p11 overexpr. TWIST/blinded/";
//inputFolderfx2 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets image1/";
//inputFolderfx2= outputfx1+ImageName+"ROI.zip"
//inputFolderfx2= outputfx1+ImageName+"ROI"
//print(inputFolderfx2);
//namefilefx2 = "LAA 100619 OMF30 p11 1-1-1.tifROI.zip";
//outputFolderfx2 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/rgb sets "+image_training_nr;


//open(inputfolderfx1+nameimagesegmented);
processfx1(inputfolderfx1,outputfx1,nameimagesegmented);


//#@ File (label = "Input directory", style = "directory") input
//#@ File (label = "Output directory", style = "directory") output
//#@ String (label = "File suffix", value = ".tif") suffix
print("hey test");
folder_clean= "C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/beta-gal counting/OMF30 p11 overexpr. TWIST/blinded/";
image_clean = image_training_name+".tif";
image_clean=folder_clean+image_clean;

inputFolderfx2= outputfx1+ImageName+"ROI";
print(inputFolderfx2);
//namefilefx2 = "LAA 100619 OMF30 p11 1-1-1.tifROI.zip";
outputFolderfx2 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/RGB sets training/";
input2 = outputfx1+ImageName+"ROI.zip";
output2 = outputFolderfx2;
processfx2(input2, output2, image_clean);

processFolderfx2(inputFolderfx2,outputFolderfx2,image_clean);
print("hey test2");

//ALL CELLS OF 1 IMAGE WITH AVERAGED COLOURS
//inputfolderfx2 = "C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/beta-gal counting/OMF30 p11 overexpr. TWIST/blinded";
//namefilefx2 = "LAA 100619 OMF30 p11 1-1-1.tifROI.zip";
//outputfolderfx2s = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/rgb__image1";


//COUNT AND CREATE ROI SEGMENTS FOR 1 IMAGE
function processfx1(input, output, file) {
	// Do the processing here by adding your own code.
	//OPEN SEGMENTED IMAGE FROM PYTHON
	open(input+file);
	rename("segmented");
	//selectWindow("LAA 100619 OMF30 p11 1-1-1.out100.tif");
	run("Out [-]");
	run("Duplicate...", "title=binary copy 1");
	run("Out [-]");
	run("Make Binary");
	run("Analyze Particles...", "  show=Overlay exclude add");
	selectWindow("segmented");
	roiManager("Show None");
	roiManager("Show All");
	roiManager("Measure");
	//then in ROI manager select more>save>name of picture like below
	//roiManager("Save", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROIset_LAA 100619 OMF30 p11 1-1-1testcp[y.zip");
	//pathout="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets image1/";
	roiManager("deselect");
	input2=output+ImageName+"ROI.zip"
	roiManager("save", output + ImageName + "ROI.zip");
	//roiManager("save", output + ImageName);
	roiManager("Delete");

	//run("script:C:\Users\048441\msc-image-counting-and-classification\2.3 FIJI code\count segmented cells.ijm.ijm.ijm");
	//Table.applyMacro("Sin=sin(row*0.1); Cos=cos(row*0.1); Sqr=Sin*Sin+Cos*Cos; ");
	//roiManager("Select", 0);
	//run("Image to Results"); what the heck does that number mean?
	// need to detect RGB pixel values of segmented cells



	// Leave the print statements until things work, then remove them.
	print("Processing: " + input + File.separator + file);
	print("Saving to: " + output);
}

//MAKE RGB FILES FOR ALL ROI OF THE IMAGE

//outputFolderfx2 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/rgb__image1/";
//selectWindow("Results");
//saveAs("Results", "C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/Chantal adipo osteo/Senescent 12 osteo/quantification/Counts_All.txt");
// function to scan folders/subfolders/files to find files with correct suffix

function processFolderfx2(input2, output2, image_clean) {
//function processFolderfx2(input2, output2, image_clean) {
	//output=outputFolderfx2;
	list = getFileList(input2);
	list = Array.sort(list);
	Array.print(list)
	suffix = ".roi" //this is kind of important though
	print("test processFolderfx2");
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input2 + File.separator + list[i]))
			processFolderfx2(input2 + File.separator + list[i], image_clean);
		if(endsWith(list[i], suffix))
			print("test 2 processFolderfx2");
			processfx2(input2, output2, list[i], i, image_clean);
			print("test 2 processFolderfx2 dit it do? processfxd");
	}

}

function processfx2(input2, output2, image_clean){
//function processfx2(input2, output2, file, cell_index, image_clean){
		//open segmented image
		//open segmented image outputted by python code
		//pathin = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/100.tif";
		//open(inputfolderfx1);
		//rename("contoured in python");
		//open CLEAN image

		//folder= "C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/beta-gal counting/OMF30 p11 overexpr. TWIST/blinded/";
		//image_clean = "LAA 100619 OMF30 p11 1-1-1.tif";
		open(image_clean);
		rename("original");
		run("Duplicate...", " ");
		rename("original copy");
		run("ROI Manager...");
		print("test enter processfx2");
	//1 CELL WITH AVERAGE COLOR

	//open ROI data for ith contoured cell
	//roiManager("Open", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/0001-0073.roi");
	//roiManager("Open", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROIset_LAA 100619 OMF30 p11 1-1-1.zip");

	//ROIzippath = outputfx1 + ImageName + "ROI.zip"
	//roiManager("Open", ROIzippath)
	//print(file);
	roiManager("Open", input2);
	//roiManager("Open", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROIset_LAA 100619 OMF30 p11 1-1-1.zip");

	//cellindex = 0;
	//list = getFileList(ROIzippath);
	//i=cell_index;
	nROIs = roiManager("count");
	cell_index=0;
	for (i = 0; i <nROIs; i++) {
	roiManager("Select", i);
	//roiManager("Select", 0);

	run("Duplicate...", " ");
	rename("object "+i);
	run("Crop");
	selectWindow("object "+i);
	//run("In [+]");
	run("In [+]");
	//run("In [+]");
	run("Clear Outside"); //make outside black for pixel counting

		// NOW PLOT RGB VLAUES TO EXCEL
		//run("Average Color", "cielab");
		w = getWidth();
		h = getHeight();
		run("Clear Results");

		j = 0;
		for (x=0; x<w; x++) {
		 showProgress(x, w);
			for (y=0; y<h; y++) {
			v = getPixel(x, y);
			r = (v & 0xff0000)>>16;
			g = (v & 0x00ff00)>>8;
			b = (v & 0x0000ff);
			//NDVI = ((r-g)/(r+g));
			//SetResult("O", i, i);
			//setResult("Pr", i, x+1);
			//setResult("Pc", i, y+1);
			//setResult("NDVI", i, NDVI);
			setResult("R",j,r);
			setResult("G",j,g);
			setResult("B",j,b);
			j++;
			}
		}
		if (w==1 && h==1) {
			setResult("R",j,r);
			setResult("G",j,g);
			setResult("B",j,b);
		}
		updateResults();
		//namefile="image"+i+"cell"+i;
		//namefile="image1cell1";
		nameSubFile = "cell "+cell_index ;

		//saveAs("Results", getDirectory("image")+"rgb__image1/"+namefile+".txt");
		//dir = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/rgb__image1/";
		//dir = output;
		//print(dir);
		saveAs("Results", output2 + image_training_nr + nameSubFile + ".txt");
		//saveAs("Results", getDirectory("image")+"rgb__image1/cell1.txt");
		run("Clear Results");
		selectWindow("object "+i);
		close();
		cell_index+=1;

			// Leave the print statements until things work, then remove them.
	//print("Processing fx2: " + input2 + File.separator + file);
	print("Saving to: " + output2);
	}
}
roiManager("deselect")
roiManager("delete")
run("Clear Results")
run("Close All")
