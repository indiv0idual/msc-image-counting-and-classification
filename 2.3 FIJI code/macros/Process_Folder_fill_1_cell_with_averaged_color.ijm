/*
 * Macro template to process multiple images in a folder
 */

#@ File (label = "Input directory", style = "directory") input
#@ File (label = "Output directory", style = "directory") output
#@ String (label = "File suffix", value = ".tif") suffix

// See also Process_Folder.py for a version of this code
// in the Python scripting language.

processFolder(input);

// function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input + File.separator + list[i]))
			processFolder(input + File.separator + list[i]);
		if(endsWith(list[i], suffix))
			processFile(input, output, list[i]);
	}
}

function processFile(input, output, file) {
	// Do the processing here by adding your own code.
//open segmented image 
//open segmented image outputted by python code 
pathin = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/100.tif";
open(pathin);

rename("contoured in python");
//open clean image
open("C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/LAA 100619 OMF30 p11 1-1-1.tif");

rename("original");
run("ROI Manager...");
//open ROI data for 1st contoured cell
roiManager("Open", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/0001-0073.roi");
//there's only one obj in ROImanager
roiManager("Select", 0);
run("Duplicate...", " ");
rename("object ");
run("Crop");
selectWindow("object ");
run("In [+]");
run("In [+]");
//run("In [+]");
run("Clear Outside"); //make outside black for pixel counting


// NOW PLOT RGB VLAUES TO EXCEL 
//run("Average Color", "cielab");
w = getWidth();
h = getHeight();
run("Clear Results");
i = 0;
for (x=0; x<w; x++) {
 showProgress(x, w);
	for (y=0; y<h; y++) {  
	v = getPixel(x, y);
	r = (v & 0xff0000)>>16;
	g = (v & 0x00ff00)>>8;      
	b = (v & 0x0000ff);            
	//NDVI = ((r-g)/(r+g));
	//SetResult("O", i, i);
	//setResult("Pr", i, x+1);
	//setResult("Pc", i, y+1);
	//setResult("NDVI", i, NDVI);
	setResult("R",i,r);
	setResult("G",i,g);
	setResult("B",i,b);
	i++;
	}
}
updateResults();
//namefile="image"+i+"cell"+i;
//namefile="image1cell1";
namefile = file ;

//saveAs("Results", getDirectory("image")+"rgb__image1/"+namefile+".txt");
dir = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/rgb__image1/";
dir = output;
//print(dir);
saveAs("Results", dir + namefile + ".txt");
//saveAs("Results", getDirectory("image")+"rgb__image1/cell1.txt");
run("Clear Results");
}
