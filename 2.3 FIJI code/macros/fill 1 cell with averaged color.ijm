//open segmented image
//open segmented image outputted by python code

pathin1 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/100.tif";
open(pathin);
rename("contoured in python");

//open CLEAN image
pathin2 = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/LAA 100619 OMF30 p11 1-1-1.tif";
open(pathin2);
//open("C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/LAA 100619 OMF30 p11 1-1-1.tif");

rename("original");
run("ROI Manager...");
//open ROI data for 1st contoured cell
roiManager("Open", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/0001-0073.roi");


// NOW PLOT RGB VLAUES TO EXCEL
//run("Average Color", "cielab");
roiManager("Select", 0);
run("Duplicate...", " ");
rename("object ");
run("Crop");
selectWindow("object ");
run("In [+]");
run("In [+]");
//run("In [+]");
run("Clear Outside"); //make outside black for pixel counting


w = getWidth();
h = getHeight();
run("Clear Results");
i = 0;
for (x=0; x<w; x++) {
 showProgress(x, w);
	for (y=0; y<h; y++) {
	v = getPixel(x, y);
	r = (v & 0xff0000)>>16;
	g = (v & 0x00ff00)>>8;
	b = (v & 0x0000ff);
	//NDVI = ((r-g)/(r+g));
	//SetResult("O", i, i);
	//setResult("Pr", i, x+1);
	//setResult("Pc", i, y+1);
	//setResult("NDVI", i, NDVI);
	setResult("R",i,r);
	setResult("G",i,g);
	setResult("B",i,b);
	i++;
	}
}
updateResults();
//namefile="image"+i+"cell"+i;
//namefile="image1cell1";
namefile = "blablabla";

//saveAs("Results", getDirectory("image")+"rgb__image1/"+namefile+".txt");
dir = "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/rgb__image1/";
//dir = output;
//print(dir);
saveAs("Results", dir + namefile + ".txt");
//saveAs("Results", getDirectory("image")+"rgb__image1/cell1.txt");
run("Clear Results");
selectWindow("object ");
close();
