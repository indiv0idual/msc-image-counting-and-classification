//open clean original image from dataset folder
pathhfolder = "C:/Users/048441/msc-image-counting-and-classification/2.2 Watershed code/";
nameofpic = "LAA 100619 OMF30 p11 1-1-1.out100.tif";
open(pathhfolder+nameofpic);
selectWindow("LAA 100619 OMF30 p11 1-1-1.out100.tif");
run("Out [-]");
run("Duplicate...", "title=binary copy 1");
run("Out [-]");
run("Make Binary");
run("Analyze Particles...", "  show=Overlay exclude add");
selectWindow("LAA 100619 OMF30 p11 1-1-1.out100.tif");
roiManager("Show None");
roiManager("Show All");
roiManager("Measure");
//then in ROI manager select more>save>name
//of picture like below to save zip
// of each ROI of this particular image
//roiManager("Save", "C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROIset_LAA 100619 OMF30 p11 1-1-1testcp[y.zip");
pathout="C:/Users/048441/msc-image-counting-and-classification/2.3 FIJI code/ROI sets image1/";
roiManager("deselect");
roiManager("save", pathout+"testing.zip");

//run("script:C:\Users\048441\msc-image-counting-and-classification\2.3 FIJI code\count segmented cells.ijm.ijm.ijm");
//Table.applyMacro("Sin=sin(row*0.1); Cos=cos(row*0.1); Sqr=Sin*Sin+Cos*Cos; ");
//roiManager("Select", 0);
//run("Image to Results"); what the heck does that number mean?
// need to detect RGB pixel values of segmented cells