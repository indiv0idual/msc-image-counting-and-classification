
import numpy as np
import sys, os
"""
#@ File    (label = "Input directory", style = "directory") srcFile
#@ File    (label = "Output directory", style = "directory") dstFile
#@ String  (label = "File extension", value=".tif") ext
#@ String  (label = "File name contains", value = "") containString
#@ boolean (label = "Keep directory structure when saving", value = true)
"""
"""
Create a numpy vector with the RGB averages of one ROI
"""
def get_RGB_raw(file):
    return np.loadtxt(file, skiprows=1, usecols=(1,2,3))
def remove_background_pixel_values(RGB_matrix):
    if (~RGB_matrix.any(axis=1)[0]):
        return np.delete(RGB_matrix, np.where(~RGB_matrix.any(axis=1))[0], axis=0)
    else:
        # print(RGB_matrix)
        return RGB_matrix
def avg_RGB(RGB_vector):
    return np.reshape(np.sum(RGB_vector, axis=0)/RGB_vector.shape[0], (1,3))
def avg_RGB_roi(filename):
    err = avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))
    return err


"""
Return one matrix with all average RGB values of cells in one image
Saves this matrix as a text file as well.
# Batch Processing an entire folder
 for 1 image only!
"""

"""
For one sub
"""
def run2(srcDir):
    suffix=".tifROI.zip"
    matrix = np.zeros((1,3))
	for root, directories, filenames in os.walk(srcDir):
        for directory in directories:
            if not directory.startswith("quantification"):
                continue

            for filename in sorted(filenames):
				if not filename.endswith(".txt"):
					continue
                matrix = np.concatenate((matrix, avg_RGB_roi(root+filename)))
		np.savetxt(srcDir+"rgb average"+root, matrix, fmt="%.2f", newline='\n')
        matrix=np.zeros((1,3))
	matrix__.append([directory[-len(suffix)], run1(os.path.get(directory))])
	return matrix__

"""
For one sub
"""
srcDir = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\counting I with ROI\OMF37 p5 silencing TWIST'
matrix__ = []
run2(srcDir)
np.savetxt(srcDir, matrix__)
