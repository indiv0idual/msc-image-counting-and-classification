
# coding: utf-8

# # Identification and Classification of histologically treated MSCs with Machine Learning techniques
# ### by Arielle Molina Rakos, BSc Nanobiology student, Erasmus University & TU Delft, december 2019

# ## Abstract

# ## 1. Introduction

# ## 2. Methods

# ### 2.1 Simple Implementation of automated counting and classification with classical Image Analysis techniques
# Different from ML, where the classifying parameters are not set by humans. In this case, we define fixed thresholds for negative, slightly positive and highly positive cells unbiased towards relative differences in color intensity per picture/sample/donor

# In[1]:


# import the necessary packages
import numpy as np
import argparse
import imutils
import cv2 
from matplotlib import pyplot as plt
import os
from tkinter.filedialog import askopenfilename, askdirectory
import sys
from ij import IJ, ImagePlus


# In[6]:


def counter_function_example(image_in, image_out):
        # -*- coding: utf-8 -*-
    """
    Bacteria counter

        Counts blue and white bacteria on a Petri dish

        python bacteria_counter.py -i [imagefile] -o [imagefile]

    @author: Alvaro Sebastian (www.sixthresearcher.com)
    

    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True,
        help="path to the input image")
    ap.add_argument("-o", "--output", required=True,
        help="path to the output image")
    args = vars(ap.parse_args())
"""
    # dict to count colonies
    counter = {}

    # load the image
    image_orig = cv2.imread(image_in)
#     height_orig, width_orig = image_orig.shape[:2]

    # output image with contours
    image_contours = image_orig.copy()

    # DETECTING BLUE AND WHITE COLONIES
    colors = ['blue', 'white']
    for color in colors:

        # copy of original image
        image_to_process = image_orig.copy()

        # initializes counter
        counter[color] = 0

        # define NumPy arrays of color boundaries (GBR vectors)
        if color == 'blue':
            lower = np.array([ 60, 100,  20])
            upper = np.array([170, 180, 150])
        elif color == 'white':
            # invert image colors
            image_to_process = (255-image_to_process)
            lower = np.array([ 50,  50,  40])
            upper = np.array([100, 120,  80])

        # find the colors within the specified boundaries
        image_mask = cv2.inRange(image_to_process, lower, upper)
        # apply the mask
        image_res = cv2.bitwise_and(image_to_process, image_to_process, mask = image_mask)

        ## load the image, convert it to grayscale, and blur it slightly
        image_gray = cv2.cvtColor(image_res, cv2.COLOR_BGR2GRAY)
        image_gray = cv2.GaussianBlur(image_gray, (5, 5), 0)

        # perform edge detection, then perform a dilation + erosion to close gaps in between object edges
        image_edged = cv2.Canny(image_gray, 50, 100)
        image_edged = cv2.dilate(image_edged, None, iterations=1)
        image_edged = cv2.erode(image_edged, None, iterations=1)

        # find contours in the edge map
        cnts = cv2.findContours(image_edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts)==2 else cnts[1]

        # loop over the contours individually
        for c in cnts:

            # if the contour is not sufficiently large, ignore it
            if cv2.contourArea(c) < 5:
                continue

            # compute the Convex Hull of the contour
            hull = cv2.convexHull(c)
            if color == 'blue':
                # prints contours in red color
                cv2.drawContours(image_contours,[hull],0,(0,0,255),1)
            elif color == 'white':
                # prints contours in green color
                cv2.drawContours(image_contours,[hull],0,(0,255,0),1)

            counter[color] += 1
            #cv2.putText(image_contours, "{:.0f}".format(cv2.contourArea(c)), (int(hull[0][0][0]), int(hull[0][0][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (255, 255, 255), 2)

        # Print the number of colonies of each color
        print("{} {} colonies".format(counter[color],color))

    # Writes the output image
    plt.imshow(image_contours)
    cv2.imwrite(image_out,image_contours)
    return


# In[7]:


path_in = r'C:\Users\048441\msc-image-counting-and-classification\2.1 Canny segmentation sixteenthresearcher\petri_dish.png'
path_out = r'C:\Users\048441\msc-image-counting-and-classification\2.1 Canny segmentation sixteenthresearcher\petri_dish.out.png'
counter_function_example(path_in, path_out)


# In[97]:


# modified version for histological results
def counter_function(image_in, image_out):
        # -*- coding: utf-8 -*-
    """
    Bacteria counter

        Counts blue and white bacteria on a Petri dish

        python bacteria_counter.py -i [imagefile] -o [imagefile]

    @author: Alvaro Sebastian (www.sixthresearcher.com)
    

    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True,
        help="path to the input image")
    ap.add_argument("-o", "--output", required=True,
        help="path to the output image")
    args = vars(ap.parse_args())
"""
    # dict to count colonies
    counter = {}

    # load the image
    image_orig = cv2.imread(image_in)
    #height_orig, width_orig = image_orig.shape[:2]

    # output image with contours
    image_contours = image_orig.copy()

    # DETECTING BLUE AND WHITE COLONIES
    colors = ['pink', 'purple', 'blue']
    for color in colors:

        # copy of original image
        image_to_process = image_orig.copy()

        # initializes counter
        counter[color] = 0

        # define NumPy arrays of color boundaries (GBR vectors)
        if color == 'pink': # unstained
            # invert image colors
            #image_to_process = (255-image_to_process)
            lower = np.array([222, 128, 217])
            upper = np.array([ 246, 175, 250])
        elif color == 'purple': # slightly stained
            # invert image colors
            #image_to_process = (255-image_to_process)
            lower = np.array([ 200,  88,  219])
            upper = np.array([232, 178,  246])
        elif color == 'blue': # strongly stained
            lower = np.array([ 121,  124,  199])
            upper = np.array([149, 155,  227])

        # find the colors within the specified boundaries
        image_mask = cv2.inRange(image_to_process, lower, upper)
        # apply the mask
        image_res = cv2.bitwise_and(image_to_process, image_to_process, mask = image_mask)

        ## load the image, convert it to grayscale, and blur it slightly
        image_gray = cv2.cvtColor(image_res, cv2.COLOR_BGR2GRAY)
        image_gray = cv2.GaussianBlur(image_gray, (5, 5), 0)

        # perform edge detection, then perform a dilation + erosion to close gaps in between object edges
        image_edged = cv2.Canny(image_gray, 50, 100)
        image_edged = cv2.dilate(image_edged, None, iterations=1)
        image_edged = cv2.erode(image_edged, None, iterations=1)

        # find contours in the edge map
        cnts = cv2.findContours(image_edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts)==2 else cnts[1]

        # loop over the contours individually
        for c in cnts:

            # if the contour is not sufficiently large, ignore it
            if cv2.contourArea(c) < 300:
                continue

            # compute the Convex Hull of the contour
            hull = cv2.convexHull(c)
            if color == 'pink':
                # prints contours in red color
                cv2.drawContours(image_contours,[hull],0,(0,0,255),1)
            elif color == 'purple':
                # prints contours in green color
                cv2.drawContours(image_contours,[hull],0,(0,255,0),1)
            elif color == 'blue':
                # prints contours in blue color
                cv2.drawContours(image_contours,[hull],0,(255,0, 0),1)

            counter[color] += 1
            #cv2.putText(image_contours, "{:.0f}".format(cv2.contourArea(c)), (int(hull[0][0][0]), int(hull[0][0][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (255, 255, 255), 2)

        # Print the number of colonies of each color
        print("{} {} colonies".format(counter[color],color))

    # Writes the output image
    plt.imshow(image_contours)
    cv2.imwrite(image_out, image_contours)
    return image_contours


# In[98]:


path_in = r'C:\Users\048441\msc-image-counting-and-classification\LAA 100619 OMF30 p11 1-1-1.tif'
path_out = r'C:\Users\048441\msc-image-counting-and-classification\2.1 sixteenthresearcher Canny segmentation\LAA 100619 OMF30 p11 1-1-1.out.tif'
counter_function(path_in, path_out)


# ### 2.2 Watershed based Segmentation
# Description comes later

# In[105]:


# import the necessary packages
import numpy as np
import argparse
import imutils
import cv2 
from matplotlib import pyplot as plt

def Watershed_counter(path_in, path_out, file):
    image_orig = cv2.imread(os.path.join(path_in,file))
    image_origg = cv2.GaussianBlur(image_orig, (0, 0), 4)

    image_gray = cv2.cvtColor(image_origg, cv2.COLOR_BGR2GRAY)
    #image_gray = cv2.GaussianBlur(image_gray, (0, 0), 4)

    ret, threshold = cv2.threshold(image_gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    plt.imshow(threshold), plt.title('OTSU threshold mask')
    plt.show()

    # noise removal (small nonzero white dots)
    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(threshold, cv2.MORPH_OPEN, kernel, iterations = 2)

    # sure background area
    sure_bg = cv2.dilate(opening,kernel,iterations=3)
#     plt.imshow(sure_bg), plt.title('sure background region 0')
#     plt.show()

    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
    #plt.imshow(dist_transform), plt.title('Distance Transform')
    #plt.show()

    ret, sure_fg = cv2.threshold(dist_transform,0.2*dist_transform.max(),255,0)
    plt.imshow(sure_fg), plt.title('sure foreground region 1')
    plt.show()

    # Finding unknown region
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg, sure_fg)
    #plt.imshow(unknown), plt.title('unknown region 2')
    #plt.show()

    # Marker labelling
    ret, markers = cv2.connectedComponents(sure_fg, connectivity=8)
    print("{} cells detected".format(ret))

    # Add one to all labels so that sure background is not 0, but 1
    markers = markers+1
    
    # Now, mark the region of unknown with zero
    markers[unknown==255] = 0
#     print("makers", markers)

    markers = cv2.watershed(image_orig,markers)
    image_orig[markers == -1] = [255,0,0]
    
    """
    Borrow Edge detection form source code (after watershed segmentation)
    and RGB interval thingie
    and drawing cell the right colour after classification
    
    """
#     # perform edge detection, then perform a dilation + erosion to close gaps in between object edges
#     image_edged = cv2.Canny(image_orig.copy(), 50, 100)
#     image_edged = cv2.dilate(image_edged, None, iterations=1)
#     image_edged = cv2.erode(image_edged, None, iterations=1)

#     # find contours in the edge map
#     image_gray_segmented = cv2.cvtColor(image_orig.copy(), cv2.COLOR_BGR2GRAY)
#     cnts = cv2.findContours(image_gray_segmented, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#     cnts = cnts[0] if len(cnts)==2 else cnts[1]
    
#     # output image with contours
#     image_contours = image_orig.copy()
    
# # loop over the contours individually
#     for c in cnts:

#         # if the contour is not sufficiently large, ignore it
#         if cv2.contourArea(c) < 300:
#             continue

#         # compute the Convex Hull of the contour
#         hull = cv2.convexHull(c)
#         if color == 'pink':
#             # prints contours in red color
#             cv2.drawContours(image_contours,[hull],0,(0,0,255),1)
#         elif color == 'purple':
#             # prints contours in green color
#             cv2.drawContours(image_contours,[hull],0,(0,255,0),1)
#         elif color == 'blue':
#             # prints contours in blue color
#             cv2.drawContours(image_contours,[hull],0,(255,0, 0),1)

#         counter[color] += 1
#         #cv2.putText(image_contours, "{:.0f}".format(cv2.contourArea(c)), (int(hull[0][0][0]), int(hull[0][0][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (255, 255, 255), 2)

#     plt.imshow(image_orig), plt.title('result')
#     plt.show()

    #path_out = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed code\LAA 100619 OMF30 p11 1-1-1.out'
    path_out_ = os.path.join(path_out, file)
    # Writes the output image
    cv2.imwrite(path_out_, image_orig)
#     cv2.imwrite(path_out, image_orig)
    # Writes the output image with a handy name CHANGE THIS NAME
#     path_out_training = path_out + "training 3.tif"
#     cv2.imwrite(path_out_training, image_orig)
    return 


# In[79]:


#path_out = path_out + str(ret) + '.tif'
#path_out = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed code\LAA 100619 OMF30 p11 1-1-1.out.tif'
#Watershed_counter(path_in, path_out)

#print(dist_transform.max(), dist_transform.min())
#print(markers.shape, image_orig.shape, ret)

#plt.imshow(image_orig),plt.title('output')


# In[50]:


path_in = r'C:\Users\048441\msc-image-counting-and-classification\\'
path_in = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting\OMF30 p11 overexpr. TWIST\blinded\\'
#path_out = path_out + str(ret) + '.tif'

Image="LAA 100619 OMF30 p11 1-1-2.tif"
path_out = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed code\\'
Watershed_counter(path_in, path_out, Image)


# In[100]:


# srcFile = askdirectory()
# srcFile = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting'
print(srcFile)
# dstFile = askdirectory()
# dstFile = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code'
print(dstFile)
ext = ".jpg" # Extension
containString = "Counter"
keepDirectories = True # Keep directory structure when saving

def run():
    srcDir=srcFile
    dstDir=dstFile
#     srcDir = srcFile.getAbsolutePath()
#     dstDir = dstFile.getAbsolutePath()
    for root, directories, filenames in os.walk(srcDir):
        filenames.sort();
        for filename in filenames:
            # Check for file extension
            if not filename.endswith(ext):
                continue
            # Check for file name pattern
            if containString in filename:
                continue
            process(srcDir, dstDir, root, filename, keepDirectories)
    return
 
def process(srcDir, dstDir, currentDir, fileName, keepDirectories):
    print("Processing: currentDir is ", currentDir)

    # Opening the image
    print("Open image file", fileName, '\n hi')
    print(os.path.join(currentDir, fileName))
    #   imp = IJ.openImage(os.path.join(currentDir, fileName))
    
    # Saving the image
    saveDir = currentDir.replace(srcDir, dstDir) if keepDirectories else dstDir
    if not os.path.exists(saveDir):
        os.makedirs(saveDir)
    
    # Put your processing commands here!
    Watershed_counter(currentDir, saveDir, fileName)
    
    print("Saving to", saveDir)
    
#     IJ.saveAs(imp, "Tiff", os.path.join(saveDir, "segmented {}".format(fileName));
#     imp.close()
 
# run()


# In[53]:


run()


# In[54]:


srcFile = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting II'
dstFile = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code'
run()


# In[106]:


srcFile = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting III Sens 18 OMF49P5'
dstFile = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting III'
run()


# In[109]:


ext = ".tif" # Extension
srcFile = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting III Sens 18 OMF49P5'
dstFile = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting III'
run()


# In[110]:


ext = ".tif" # Extension
srcFile = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting IV Sens 20 OMF42P5'
dstFile = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting IV'
run()


# In[111]:


srcFile = r'C:\Users\048441\msc-image-counting-and-classification\fotos voor wendy & Chantal\beta-gal counting II\OMF38 p7 silencing'
dstFile = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\beta-gal counting II'
run();


# In[37]:



import numpy as np
import sys, os
"""
#@ File    (label = "Input directory", style = "directory") srcFile
#@ File    (label = "Output directory", style = "directory") dstFile
#@ String  (label = "File extension", value=".tif") ext
#@ String  (label = "File name contains", value = "") containString
#@ boolean (label = "Keep directory structure when saving", value = true) 
"""
"""
Create a numpy vector with the RGB averages of one ROI
"""
def get_RGB_raw(file):
    return np.loadtxt(file, skiprows=1, usecols=(1,2,3))
def remove_background_pixel_values(RGB_matrix):
    if (~RGB_matrix.any(axis=1)[0]):
        return np.delete(RGB_matrix, np.where(~RGB_matrix.any(axis=1))[0], axis=0)
    else:
        # print(RGB_matrix)
        return RGB_matrix
def avg_RGB(RGB_vector):
    return np.reshape(np.sum(RGB_vector, axis=0)/RGB_vector.shape[0], (1,3))
def avg_RGB_roi(filename):
    err = avg_RGB(remove_background_pixel_values(get_RGB_raw(filename)))
    return err



"""
Return one matrix with all average RGB values of cells in one image
Saves this matrix as a text file as well.
# Batch Processing an entire folder
 for 1 image only!
"""
def run1(srcDirec):

    matrix = np.zeros((1,3))
    for root, directories, filenames in os.walk(srcDirec):
        for filename in sorted(filenames):
            if not filename.endswith(".txt"):
                continue
            
            matrix = np.concatenate((matrix, avg_RGB_roi(os.path.join(srcDirec,filename))))
            head,tail=os.path.split(srcDirec)
    np.savetxt("RGB mean + {}.txt".format(tail[-20:].replace(suffix, "")), matrix, fmt="%.2f", newline='\n')
    return matrix

"""
For one sub
"""
def run2(srcDir):
    suffix=".tifROI.zip"
    for root, directories, filenames in os.walk(srcDir):
#        filenames.sort();
        for directory in directories:
            # Check for file extension
            if directory.startswith("quantification"):
    #           head, tail = os.path.split(directory)    
    #           tail.replace(suffix, "")
                direc=os.path.join(srcDir,directory)
                print(direc)
                matrix__.append([directory[-20:].replace(suffix, ""), run1(direc)])
    return 

"""
For one sub
"""
suffix=".tifROI.zip"
srcDir = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed segmented images & code\counting I with ROI\OMF30 p11 overexpr. TWIST' 
matrix__ = []
run2(srcDir)


# In[ ]:


counting1omf30p11= matrix__


# In[ ]:


"""
finalize classifying what needs to be classified
"""

