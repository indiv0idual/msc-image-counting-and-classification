# import the necessary packages
import numpy as np
import argparse
import imutils
import cv2
from matplotlib import pyplot as plt

def Watershed_counter(path_in, path_out,file):
    image_orig = cv2.imread(path_in+file)
    image_origg = cv2.GaussianBlur(image_orig, (0, 0), 4)

    image_gray = cv2.cvtColor(image_origg, cv2.COLOR_BGR2GRAY)
    #image_gray = cv2.GaussianBlur(image_gray, (0, 0), 4)

    ret, threshold = cv2.threshold(image_gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    plt.imshow(threshold), plt.title('OTSU threshold mask')
    plt.show()

    # noise removal (small nonzero white dots)
    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(threshold, cv2.MORPH_OPEN, kernel, iterations = 2)

    # sure background area
    sure_bg = cv2.dilate(opening,kernel,iterations=3)
    plt.imshow(sure_bg), plt.title('sure background region 0')
    plt.show()

    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
    #plt.imshow(dist_transform), plt.title('Distance Transform')
    #plt.show()

    ret, sure_fg = cv2.threshold(dist_transform,0.2*dist_transform.max(),255,0)
    #plt.imshow(sure_fg), plt.title('sure foreground region 1')
    #plt.show()

    # Finding unknown region
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg, sure_fg)
    #plt.imshow(unknown), plt.title('unknown region 2')
    #plt.show()

    # Marker labelling
    ret, markers = cv2.connectedComponents(sure_fg, connectivity=8)
    print("{} cells detected".format(ret))

    # Add one to all labels so that sure background is not 0, but 1
    markers = markers+1

    # Now, mark the region of unknown with zero
    markers[unknown==255] = 0

    markers = cv2.watershed(image_orig,markers)
    image_orig[markers == -1] = [255,0,0]

#      # loop over the contours individually
#     for c in :

#         # if the contour is not sufficiently large, ignore it
#         if cv2.contourArea(c) < 300:
#             continue

    plt.imshow(image_orig), plt.title('result')
    plt.show()

    #path_out = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed code\LAA 100619 OMF30 p11 1-1-1.out'
    path_out_ = path_out + str(ret) + file
    # Writes the output image
    cv2.imwrite(path_out_, image_orig)
    # Writes the output image with a handy name
    path_out_training = path_out + "training 2.tif"
    cv2.imwrite(path_out_training, image_orig)
    return dist_transform.max()

path_in = r'C:\Users\048441\msc-image-counting-and-classification\\'
#path_out = path_out + str(ret) + '.tif'

Image="LAA 100619 OMF30 p11 1-1-2.tif"
path_out = r'C:\Users\048441\msc-image-counting-and-classification\2.2 Watershed code\\'
Watershed_counter(path_in, path_out, Image)
