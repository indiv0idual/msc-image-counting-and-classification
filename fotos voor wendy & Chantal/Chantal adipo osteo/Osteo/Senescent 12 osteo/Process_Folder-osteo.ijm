setBatchMode(true);
/*
 * Macro template to process multiple images in a folder
 */

#@ File (label = "Input directory", style = "directory") input
#@ File (label = "Output directory", style = "directory") output
#@ String (label = "File suffix", value = ".tif") suffix

// See also Process_Folder.py for a version of this code
// in the Python scripting language.
input="C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/Chantal adipo osteo/Senescent 12 osteo/";
output="C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/Chantal adipo osteo/Senescent 12 osteo/quantification/";

processFolder(input);
//selectWindow("Results");
//Table.applyMacro("Foreground=Mean/2.55; ");
//saveAs("results", "C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/Chantal adipo osteo/Senescent 12 osteo/quantification/foreground_percentage.txt");


// function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input + File.separator + list[i]))
			processFolder(input + File.separator + list[i]);
		if(endsWith(list[i], suffix))
			processFile(input, output, list[i]);
	}
		
}

function processFile(input, output, file) {
	// Do the processing here by adding your own code.
	open(input+file);
	run("Subtract Background...", "rolling=50 light");
	run("Make Binary");
	//getStatistics(area, mean, min, max, std, histogram);
	//run("Set Measurements...", "area mean");
	//run("Measure");
	//run("Histogram");
	saveAs("Results", input+"blobs.csv");
	nBins=256;
	//getHistogram(values, counts, nBins);
	run("Clear Results");
   getHistogram(values, counts, 256);
   for (row=0; row<256; row++) {
       setResult("Value", row, row);
       setResult("Count", row, counts[row]);
   }
   updateResults();
   Table.deleteRows(1, 254);
   area = 3117973+19563;
   Table.applyMacro("Foreground=Count/2.55; ");
   saveAs("results", output+file+"fg.txt");
   print(Table.get("Count", 1));
	//setResult("Value", 255, values[255]);
	//fg_pixels = Table.get("Count", 255);
	//File.append(fg_pixels+" \n", input+"foreground_percentages.txt");
	//file.app
	selectWindow(file);
	close();
	

//run("Clear Results");
	// Leave the print statements until things work, then remove them.
	print("Processing: " + input + File.separator + file);
	print("Saving to: " + output);

}
run("Close All");