setBatchMode(true);
/*
 * Macro template to process multiple images in a folder
 */

#@ File (label = "Input directory", style = "directory") iinput
//#@ File (label = "Output directory", style = "directory") output
//#@ String (label = "File suffix", value = ".jpg") suffix


suffix = ".tif"
print("iinput is "+iinput);
iinput;
print("iinput is "+iinput);
input = replace(iinput, "\\", "/") + "/";
print("input after replacement is "+input);
hi=0;

processFolder(input);


// function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	hi++;
	list = Array.sort(list);
	
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(input + File.separator + list[i])) { // directory means folder
			//output = File.makeDirectory(input + File.separator + list[i] + "quantification"
			if(indexOf(list[i], "quantification") >= 0) || (list[i] == input + File.separator + list[i]))
				//exit("rename or remove previous quantification folder first");
				continue;
			output = input + "quantification " + list[i];
			if (File.isDirectory(output)=true)
				output += " kopy"
			File.makeDirectory(output);
			processFolder(input + File.separator + list[i]);
		}
		if(endsWith(list[i], suffix))
			processFile(input, output, list[i]);
	}
	if (isOpen("Results")){
		selectWindow("Results");
		saveAs("results", output +" foreground_percentage.txt");
		Table.rename("Results", File.getName(output));	
	}
}

function processFile(input, output, file) {
	// Do the processing here by adding your own code.
	open(input+file);
	rename(file);
	run("Subtract Background...", "rolling=50 light");
	run("32-bit");
	//run("Threshold...");
	//setOption("BlackBackground", true);
	run("Select All");
	setThreshold(1, 225.32);
	//run("Apply Threshold");
	run("Convert to Mask");
	//setAutoThreshold("Default no-reset");
	run("Set Measurements...", "area mean area_fraction display redirect=None decimal=3");
	run("Measure");
	selectWindow(file);
	saveAs("png", output+file+"calcification mask.jpg");
	close();
//run("Clear Results");
// Leave the print statements until things work, then remove them.
	print("Processing: " + input + File.separator + file);
	print("Saving to: " + output);
	
	
}
run("Close All");