//setBatchMode(true);
/*
 * Macro template to process multiple images in a folder
 */

#@ File (label = "Input directory", style = "directory") input
#@ File (label = "Output directory", style = "directory") output
#@ String (label = "File suffix", value = ".jpg") suffix

// See also Process_Folder.py for a version of this code
// in the Python scripting language.
//input="C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/Chantal adipo osteo/OMF46 p6 adipo + TNF/";
//output="C:/Users/048441/msc-image-counting-and-classification/fotos voor wendy & Chantal/Chantal adipo osteo/OMF46 p6 adipo + TNF/quantification/";
//output=input;
l=0;
processFolder(input);


// function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list);

	for (i = 0; i < list.length; i++) {
		
		if(File.isDirectory(input + File.separator + list[i])){
			processFolder(input + File.separator + list[i]);
			selectWindow("Results");
			Table.rename("Results", "list[i]");
			saveAs("results", list[i] + ".txt");
		}
		if(endsWith(list[i], suffix))// && startsWith(list[i], prefix))
			processFile(input, input, list[i]);
			//if(i==list.length-1)
				//selectWindow("Results");
				//saveAs("results", input + File.separator + list[i]+".txt");
				//run("Clear Results");	
		
	
	}


		
}

function processFile(input, output, file) {
	setBatchMode(true); 
	// Do the processing here by adding your own code.
	open(input+file);
	//rename(file);
	run("Subtract Background...", "rolling=50 light");
	run("RGB Stack");
	run("Stack to Images");
	selectWindow("Blue");
	rename("Blue " + file);
	run("Select All");
	setThreshold(0, 135);
	run("Set Measurements...", "area mean area_fraction display redirect=None decimal=3");
	run("Measure");
	//selectWindow(file);
	saveAs("png", output+ "fat blobs mask" + file + ".jpg");
	close();
//run("Clear Results");
// Leave the print statements until things work, then remove them.
	print("Processing: " + input + File.separator + file);
	print("Saving to: " + output);
	
	
}
run("Close All");