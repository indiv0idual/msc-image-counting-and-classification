rename("image now clean");
run("Out [-]");
run("8-bit");
run("Undo");
run("Duplicate...", "title=[image now clean coloured edges]");
run("Find Edges");
run("Gaussian Blur...", "sigma=7");


selectWindow("image now clean");
run("Duplicate...", "title=[grayscale blurred thresh]");
run("Out [-]");
run("8-bit");
run("Gaussian Blur...", "sigma=2");
setAutoThreshold("Otsu");
//run("Convert to Mask");

selectWindow("image now clean");
run("Duplicate...", "title=[copy image now clean]");
selectWindow("image now clean");
run("Duplicate...", "title=[dup]");
run("Enhance Contrast", "saturated=0.35");
run("Convert to Mask");
run("Apply LUT");
//run("Find Maxima...", "prominence=40 light output=Count");
run("Invert");
imageCalculator("Subtract create", "copy image now clean","dup");